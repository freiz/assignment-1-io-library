section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall


; char *str = "this is line";
; string_length(str);
; div rbx; rax - частное; rdx - остаток
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax 

.loop:
    mov sil, byte[rdi+rax]
    test sil, sil
    jz .end
    inc rax
    jmp .loop

.end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rbx, rdi
    call string_length
    mov rdi, 1
    mov rsi, rbx
    mov rdx, rax
    mov rax, 1
    syscall
    xor rbx, rbx
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA) 
print_newline:
    mov rdi, 10
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov rsi, 1
    push rbx
    dec rsp
    mov rbx, 0xA

.loop:
    xor rdx, rdx
    div rbx
    add rdx, 0x30  
    dec rsp
    mov [rsp], dl
    inc rsi
    test rax, rax
    jnz .loop

.print_d:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .positive
    push rdi
    mov rdi, 0x2D ; '-'
    call print_char
    pop rdi
    neg rdi
    
.positive:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax

.loop:
    mov bl, [rdi+rax]
    cmp bl, byte[rsi+rax]
    jne .noequals
    cmp bl, 0
    test bl, bl
    je .equals
    inc rax
    jmp .loop

.noequals:
    xor rax, rax
    ret

.equals:
    mov rax, 1
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rsi, rsp 
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    syscall
    test rax, rax
    jz .stop
    mov al, [rsp]

.stop:
    inc rsp
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx   
    dec rsi   

.loop:
    cmp rdx, rsi
    jge .stop
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    test rax, rax
    jz .terminate
    push rax
    push rdi

    mov dil, al
    call .is_space
    test rax, rax
    pop rdi
    pop rax
    jz .if_not_space
    test rdx, rdx
    jz .loop
    jmp .terminate

.is_space:
    cmp dil, 0xA
    je .true
    cmp dil, 0x20
    je .true
    cmp dil, 0x9
    je .true

.false:
    mov rax, 0
    ret

.true:
    mov rax, 0xF
    ret

.if_not_space:
    mov [rdi+rdx], rax
    inc rdx
    jmp .loop

.terminate:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret
.stop:
    xor rax, rax
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, 10;

.loop:
    push rsi
    mov sil, [rdi]
    test sil, sil
    je .stop
    cmp sil, 0xA
    je .stop
    cmp sil, 0x30
    jb .stop
    cmp sil, 0x39
    ja .stop
    sub sil, 0x30
    mul r8
    add rax, rsi
    inc rdi
    pop rsi
    inc rsi
    jmp .loop

.stop:
    pop rdx
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi;
    call parse_uint
    pop rdi
    test rdx, rdx
    jne .stop
    mov sil, [rdi]
    inc rdi
    push rsi
    call parse_uint
    inc rdx
    pop rsi
    cmp sil, 0x2d
    jne .stop
    neg rax
    jne .stop

.stop:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .end
.loop:
    mov dl, [rdi+r8]
    mov [rsi+r8], dl
    inc r8
    cmp r8, rax
    jle .loop
.end:
    xor rax, rax
    ret
